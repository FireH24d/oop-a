package week3.sss;

public class Square extends Shape{
    int side=5;
    @Override
    void area() {
        System.out.println(side*side);
    }

    @Override
    void display() {
        System.out.println("Square");
    }
}
