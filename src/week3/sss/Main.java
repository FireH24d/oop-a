package week3.sss;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Shape circle= new Circle();
        Shape square=new Square();

        List<Shape> createdShapes=new ArrayList<>();
        createdShapes.add(circle);
        createdShapes.add(square);
        for(Shape shape:createdShapes){
            shape.display();
        }

    }
}
