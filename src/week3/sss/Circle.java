package week3.sss;

public class Circle extends Shape {
    int radius=10;
    @Override
    void area() {
        System.out.println(radius*radius*3.14);
    }

    @Override
    void display() {
        System.out.println("Сircle");
    }
}
