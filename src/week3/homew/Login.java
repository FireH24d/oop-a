package week3.homew;

import java.util.regex.Pattern;

public class Login {
    String name;
    int age;
    String mail;
    String password;
    boolean isAgeappropriate;
    boolean isThereMail;
    boolean isPasswordappropriate;

    public Login(String name) {
        this.name = name;
    }

    public void putAge(int age) {
        System.out.println(age);
        this.age = age;
        if(age>10 &&age<100){
            isAgeappropriate=true;
        }
        else{
            isAgeappropriate=false;
        }
    }

    public void putMail(String mail) {
        System.out.println(mail);
        this.mail = mail;
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (mail == null)
            isThereMail= false;
        isThereMail= pat.matcher(mail).matches();

}

    public void putPassword(String password) {
        System.out.println(password);
        this.password = password;
        if(password!=null){
            isPasswordappropriate=true;
        }
        else isPasswordappropriate=false;
    }

    public void login() throws LogException {

        if (isPasswordappropriate && isThereMail && isAgeappropriate) {
            System.out.println("Successful login,"+name+", I guess! Congratulation! It's a celebration!");//reference to ...
        } else {
            throw new LogException( name + " smth is wrong!");
        }
    }

}
