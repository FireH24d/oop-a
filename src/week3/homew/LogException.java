package week3.homew;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class LogException extends Exception {
    public LogException(String message){

        super(message);

        PrintWriter e = null;
        try {
            FileOutputStream myFile = new FileOutputStream("text.txt");
            e = new PrintWriter(myFile);
            e.print(message);
            e.close();
        } catch (IOException a) {
            System.out.println("Nothing to write!");
        } finally {
            e.close();
        }
    }


}

