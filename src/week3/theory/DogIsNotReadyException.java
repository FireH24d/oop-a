package week3.theory;

public class DogIsNotReadyException extends Exception {
    public DogIsNotReadyException(String message){
        super(message);
    }

}
