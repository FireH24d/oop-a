package week9;

public class Main {
    public static void main(String[] args) {
        Factory factory=new Factory();
        Mobile a=factory.make("Samsung");
        a.display();
        Mobile b=factory.make("IPhone");
        b.display();
        Mobile c=factory.make("Xiaomi");
        c.display();
        Mobile d=factory.make("Sony");
    }
}
