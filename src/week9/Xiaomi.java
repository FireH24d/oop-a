package week9;

public class Xiaomi implements Mobile {
    private String name;
    private int price;

    public Xiaomi(String name, int price) {
        this.name = name;
        this.price = price;
    }
    @Override
    public void display(){
        System.out.println("Mobile is Xiaomi Name:"+name+" Price:"+price);
    }
}
