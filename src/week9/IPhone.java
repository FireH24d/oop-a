package week9;

public class IPhone implements Mobile {
    private String name;
    private int price;

    public IPhone(String name, int price) {
        this.name = name;
        this.price = price;
    }
    @Override
    public void display(){
        System.out.println("Mobile is IPhone Name:"+name+" Price:"+price);
    }
}
