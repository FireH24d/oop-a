package week9;

public class Factory {
    public Mobile make(String phone){
        try
        {
        switch(phone){
            case "IPhone": return new IPhone("IPhone XX",55555);
            case "Samsung": return new Samsung("Samsung S9+",320000);
            case "Xiaomi": return new Xiaomi("Sony Xperia 1 II",8888);
            default: throw new Exception("It is not from our catalog");
        }
        }
        catch(Exception ex)
            {
                System.out.println(ex.getMessage());
            }
        return null;
    }
}
