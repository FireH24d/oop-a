package week1.SMTHS;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        File file1 = new File("src\\SMTHS\\f.txt");
        File file2 = new File("src\\SMTHS\\f.txt");

        BufferedReader br = new BufferedReader(new FileReader(file1));

        String st;
        while ((st = br.readLine()) != null) {
            System.out.println(st);
        }

        if (file2.createNewFile())
        {
            System.out.println("File is created!");
        } else {
            System.out.println("File already exists.");
        }


        FileWriter writer = new FileWriter(file2);
        while ((st = br.readLine()) != null) {
            writer.write(st);
        }
        writer.close();
    }

}
