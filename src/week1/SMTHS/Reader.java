package week1.SMTHS;


import java.io.*;

public class Reader {


    public String read() {
        String myText = "";

        char lf =  0x0A;
        String endLine = ""+lf;
        try (FileInputStream myFile = new FileInputStream("src\\SMTHS\\ddd.txt");
             InputStreamReader inputStreamReader = new InputStreamReader(myFile, "UTF-8");
             BufferedReader reader = new BufferedReader(inputStreamReader)) {
            String nextLine;
            boolean eof = false;
            while (!eof) {
                nextLine = reader.readLine();
                if (nextLine == null){
                    eof = true;
                } else {
                    myText += nextLine+endLine;
                    // myTextArray.add(nextLine);
                }
            }        System.out.println(myText);

        }
        catch (IOException e){
            System.out.println("Can't read Stalking.txt");
        }
        return myText;
    }
    public void out(String text){
        try {
            System.out.println(text);
            FileOutputStream myFile = new FileOutputStream("src\\SMTHS\\f.txt");
            PrintWriter out = new PrintWriter(myFile);
            out.print(text);
            out.close();
        } catch (IOException e){
            System.out.println("Don't write date to File");
        }
    }

}
