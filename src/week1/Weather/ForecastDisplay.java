package week1.Weather;

public class ForecastDisplay implements Observer, DisplayElement {
    private float cPressure = 29.92f;
    private float lPressure;
    private WeatherData w;

    public ForecastDisplay(WeatherData weatherData) {
        this.w = weatherData;
        weatherData.registerObserver(this);
    }
@Override
    public void update(float temp, float humidity, float pressure) {
        lPressure = cPressure;
        cPressure = pressure;
        display();
    }
@Override
public void display() {
    System.out.print("Forecast: ");
    if (cPressure > lPressure) {
        System.out.println("Improving weather on the way!");
    } else if (cPressure == lPressure) {
        System.out.println("More of the same");
    } else if (cPressure < lPressure) {
        System.out.println("Watch out for cooler, rainy weather");
    }
}
}