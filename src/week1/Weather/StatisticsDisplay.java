package week1.Weather;

public class StatisticsDisplay implements Observer, DisplayElement {
    private float maxtemp = 0.0f;
    private float mintemp = 200;
    private float tempsum = 0.0f;
    private int num;
    private WeatherData w;

    public StatisticsDisplay(WeatherData w) {
        this.w = w;
        w.registerObserver(this);
    }
    public void update(float temp, float humidity, float pressure) {
        tempsum += temp;
        num++;

        if (temp > maxtemp) {
            maxtemp = temp;
        }

        if (temp < mintemp) {
            mintemp = temp;
        }

        display();
    }
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempsum / num)
                + "/" + maxtemp + "/" + mintemp);
    }
}