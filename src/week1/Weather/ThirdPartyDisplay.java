package week1.Weather;

public  class ThirdPartyDisplay implements Observer, DisplayElement{
    float temp;
    float humidity;
    float pressure;
    private WeatherData weatherData;

    public ThirdPartyDisplay(WeatherData weatherData){
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }
    @Override
    public void display() {
        System.out.println("ThirdParty is " + temp + " pressure is " + pressure);

    }

    @Override
    public void update(float temp, float humidity, float pressure) {
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;

        display();
    }
}