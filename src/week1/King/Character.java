package week1.King;

public abstract class Character {
    WeaponBehavior weapon;

    public void Fight() {
        weapon.UseWeapon();
    }
    public void SetWeapon(WeaponBehavior w) {
        this.weapon = w;
    }
}