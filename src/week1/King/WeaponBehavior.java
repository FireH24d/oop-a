package week1.King;

public interface WeaponBehavior
{
    void UseWeapon();
}