package week1.PW;

public class Employee {
    private String name;
    private static double rate;
    private static int hours;
    public static int count = 0; //кол-во типо employee
    public static double totalsum = 0; //сумма зарплат всех участников

//constructor
    public  Employee (){
        count++;
    }
    //with 2 parameters constructer
    public  Employee(String name, int rate) {
        this.name = name;
        this.rate = rate;
        count++;
    }
    //with 3 parameters
    public Employee(String name, int rate, int hours) {
        this.name = name;
        this.rate = rate;
        this.hours = hours;
        count++;
        totalsum=totalsum+Salary();
    }
//methods
    public static double Salary() {

        return rate * hours;
    }
    public  void changeRate(double rate){
        totalsum=totalsum-this.rate*hours+rate*hours;
setRate(rate);
    }

    public  static  double bonuses() {
        return  Salary() / 10;
    }
    public static int TotalNo() {

        return count;
    }
    @Override
    public String toString() {
        return "Employee [name="+name+", rate="+rate+", hours="+hours+"]";


    }
    public static double totalSum() {
        return totalsum;
    }

    //Getter and Setter
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public double getRate() {

        return rate;
    }

    public void setRate(double rate) {

        this.rate = rate;
    }

    public static int getHours() {

        return hours;
    }

    public  void setHours(int hours) {

        this.hours = hours;
    }




}
