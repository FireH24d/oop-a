package week1.HW;

    public class Person {
        private  String name;
        private  int birthYear;

        //default constr
        public Person() {
        }
        //with 2 param
        public  Person(String name, int birthYear) {
            this.name = name;
            this.birthYear = birthYear;
        }
//Methods
        public int age(){
            return 2020 - birthYear;
        }
        public void output(){
            System.out.println("Name: " + name + ", by: " +birthYear);
        }
        public void input(String name, int birthYear){
            this.name = name;
            this.birthYear = birthYear;
        }

        @Override
        public String toString(){
            return "Person [ name: " + name+", birth year: "+birthYear+"]";
        }
//Getter  and Setter
        public void changeName(String Nname){
            name = Nname;
        }


        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

        public int getbirthYear() {
            return birthYear;
        }

        public void setbirthYear(int birthYear) {
            this.birthYear = birthYear;
        }




}
