package week1.HW;

public class Main {
    public static void main(String[] args) {
        Person a = new Person();
        Person b = new Person();
        Person c = new Person();
        Person d = new Person();
        Person e = new Person();
        Person f = new Person("SSSSS",2000);

        a.input("Ayan", 1999);
        b.input("Bek", 2001);
        c.input("Serik", 2002);
        d.input("Ermek", 2003);
        e.input("AAAAA", 2004);
        e.toString();
        System.out.println(e.age());//16
        System.out.println(d.age());//17

        e.output();//AAAAA
        e.changeName("SSSS");

        e.output();//SSSS
        f.output();

    }
}
