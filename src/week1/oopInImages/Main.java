package week1.oopInImages;

public class Main {
    public static void main(String[] args) {
        Gun glock=new Gun(18);
        Gun ak47=new Gun(30);
        Transformer optimus =new Transformer(0);
        Transformer Bunmblebee = new Transformer(1);
        optimus.run();
        optimus.gunInLeftHand=glock;
        optimus.gunInRightHand=ak47;
        optimus.fire();
        Bunmblebee.gunInLeftHand=glock;
        Bunmblebee.gunInRightHand=ak47;
        Bunmblebee.fire();
        System.out.println(optimus.position);
    }

}
