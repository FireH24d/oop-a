package week1.oopInImages;

public class Transformer {
    int position;
    Gun gunInLeftHand;
    Gun gunInRightHand;
    public Transformer(int position){
        this.position=position;
    }
    public void fire(){
        gunInRightHand.fire();
        gunInLeftHand.fire();
    }
    public void run(){
        position=position+1;
    }
}
