package week1.smth;

public class FlyNoWay implements Flyable {
    @Override
    public void fly(int time) {
        System.out.println("I am not flying");
    }
}
