package week1.smth;

public abstract class Duck {
    Quackable quackBehavior;
    Flyable flyBehavior;

    public void performQuack(){
        quackBehavior.quack();
    }
    public void performFly(int time){
        flyBehavior.fly( time);
    }
    public void swim(){}
    public void quack(){
        System.out.println("Quack");
    }
    public void display(){
        System.out.println("I am a duck");
    }

}
