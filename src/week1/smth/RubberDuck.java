package week1.smth;

public class RubberDuck extends Duck  {
    public RubberDuck(){
        quackBehavior=new Squeck();
        flyBehavior=new FlyNoWay();
    }

}
