package week1.smth;

public class Squeck implements Quackable {
    @Override
    public void quack() {
        System.out.println("I am squeking");
    }
}
